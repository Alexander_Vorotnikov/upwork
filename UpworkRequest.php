<?php

class UpworkRequest
{
    /**
     * @var resource
     */
    private $curl;
    
    private const URL = 'https://www.upwork.com/search/jobs/url';
    private const HEADERS = [

        'x-requested-with: XMLHttpRequest',
        'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:76.0) Gecko/20100101 Firefox/76.0',
        ];

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->curl = curl_init();

        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
    }
 
    public function searchJobs(array $params): ?array
    {
        $params = http_build_query($params);

        curl_setopt($this->curl, CURLOPT_URL, self::URL . '?' . $params);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, self::HEADERS);

        $response = curl_exec($this->curl);

        return json_decode($response, true);
    }
    public function __destruct()
    {
        curl_close($this->curl);
    }
}