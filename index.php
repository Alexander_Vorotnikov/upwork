<?php

require_once 'db.php';
require_once 'UpworkRequest.php';

$request = new UpworkRequest();

$perPage = readline('Select the number of jobs to show: ');
$q = readline('What kid of job are you looking for: ');

$upworkResponse = $request->searchJobs([
    "per_page" => $perPage,
    'q' => $q,
    'sort' => 'recency',
]);

$jobs = $upworkResponse['searchResults']['jobs'];

$toSaveJobs = [];
foreach ($jobs as $job) {
    $url = substr($upworkResponse['url'], 13);
    $toSaveJobs[] = [
        'job_id' => $job['ciphertext'],
        'title' => trim(strip_tags($job['title'] ?? '')),
        'description' => trim(strip_tags($job['description'] ?? '')),
        'fixed_budget' => $job['amount']['amount'] ?: null,
        'hourly_budget' => $job['hourlyBudgetText'] ?? '',
        'weekly_budget' => $job['weeklyBudget']['amount'] ?: null,
        'job_link' => "https://www.upwork.com/search/jobs/details/{$job['ciphertext']}/$url",

    ];
}

$inserts = [];
foreach ($toSaveJobs as $fields) {
    $plainFields = '';
    foreach ($fields as $field) {
        if (!$field) {
            $plainFields .= 'null,';
            continue;
        }

        if (is_numeric($field)) {
            $plainFields .= sprintf('%s,', $field);
            continue;
        }

        $plainFields .= sprintf('"%s",', $field);
    }

    $inserts[] = sprintf('(%s)', rtrim($plainFields, ','));
}

$sql = sprintf(
    'INSERT IGNORE INTO job_info (job_id, title, description, fixed_budget, hourly_budget, weekly_budget, job_link) VALUES %s',
    implode(',', $inserts)
);

$db->prepare($sql)->execute();





//   [proposalsTier] => 10 to 15


